quote = "I changed my password to incorrect."

io.write("Quote length ", string.len(quote), "\n")
io.write("Replace I with me ", string.gsub(quote, "I", "me"), "\n")

-- repeat do while
repeat
	io.write("Enter your guests: ")
	guess = io.read()

until tonumber(guess) == 15

for i = 1, 10, 1 do
	io.write(i)
end
