aTable = {}

for i = 1, 10 do
	aTable[i] = i
end

io.write("First :", aTable[1],"\n")

io.write("Number of tables: ", #aTable,"\n")

table.insert(aTable,1,100);

print(table.concat(aTable,", "))

aMultiable = {}

for i=0,9 do
	aMultiable[i] ={}
	for j = 0, 9 do
		aMultiable[i][j] = tostring(i) .. tostring(j)
	end
end

io.write("Table [0][0]", aMultiable[0][0], "\n")
