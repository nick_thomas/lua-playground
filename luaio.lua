-- Differentr ways to work with files
-- r : Read only
-- w : Overwrite or create a new file
-- a : Append or create a new file
-- r+ Read & Write existing file
-- w+ : OVerwrite read or create a file
-- a+ : Append read or create file

file = io.open("test.lua", "w+")

file:write("Random String of text\n")
file:write("Some more text \n")

file:seek("set",0)

print(file:read("*a"))

file:close()

file = io.open("text.lua","a+")

file:write("Even more text\n")

file:seek("set",0)

print(file:read("*a"))

file:close()
